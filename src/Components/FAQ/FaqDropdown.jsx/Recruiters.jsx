import React, { useState } from 'react';
import './Faq.css'


const Recruiters = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className='DropDown'>
  <button onClick={() => setIsOpen(!isOpen)}>
How can I share my portfolio with Recruiters?</button>
{isOpen && (
<div className='Dropdown-items'>                        
<p>You can generate a direct link to your portfolio from your account and share it with recruiters 
via email, social media, or in your resume.</p>
</div>
)}
    </div>
  );
}

export default Recruiters;
