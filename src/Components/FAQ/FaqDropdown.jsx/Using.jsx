import React, { useState } from 'react';
import './Faq.css'


const Using = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className='DropDown'>
  <button className="Dropdown-button" onClick={() => setIsOpen(!isOpen)}>
  How do I start using intevoo?
</button>
{isOpen && (
  <div className='Dropdown-items'>                        
<p>To get started, go to our Web App at https://intevoo.app/ and create a free account. 
Once logged in, you can choose from our professional templates and start customizing 
your digital portfolio.</p>
<ul>
<li>Create your account https://intevoo.app/register</li>
<li>Choose your model https://intevoo.app/design</li>
<li>Customize template
</li>
</ul>
</div>
)}
    </div>
  );
}

export default Using;
