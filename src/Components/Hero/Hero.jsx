import React from 'react';
import './Home.css'
import { Link } from 'react-router-dom';
import Using from '../FAQ/FaqDropdown.jsx/Using';
import Recruiters from '../FAQ/FaqDropdown.jsx/Recruiters';
import Faq from '../FAQ/Faq';
import Contacts from '../Contact/Contacts';

const Home = () => {
  return (
    <div className='Hero'>
      <div className='Hero-flex'>
      <div className='Hero-left'>
        <h1>We build digital CVs, to highlight you</h1>
        <p>Create a free digital portfolio or CV using our pre-designed professional templates.</p>

        <span>
          <Link to='/about'>Create an account</Link>
        </span>

      </div>


      <div className='Hero-right'>
        <img src='https://intevoo.app/assets/main/img/Illus%20Png/Png.png' alt='digital'/>
      </div>

      </div>
      

      
      



    </div>
  );
}

export default Home;
