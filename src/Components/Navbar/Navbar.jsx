import React from 'react';
import './Navbar.css';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';



const Navbar = () => {
  return (
    <div className='Navbar'>
    <div className='Left-nav'>
    <Link to='/' index>
      <img src='https://intevoo.app/assets/main/img/logo//without.png' alt='Logo'/>
    </Link>
<div className='hidden'>
<Link to='/' index>Home</Link>
<Link to='/about'>About</Link>
<Link to='/contact'>Contact</Link>
</div>
    </div>
    <div className='Right-nav'>
    <Button variant="warning">Login</Button>
    <Link to='' index>
      <img src='https://img.icons8.com/emoji/48/null/france-emoji.png' alt='French'/>
    </Link>

    <Link to='' index>
      <img src='https://img.icons8.com/emoji/48/null/united-states-emoji.png' alt='English'/>
    </Link>

    </div>

    

    </div>
  );
}

export default Navbar;
